# quick-crop

A small python script that facilitates the removal of black bars in videos.

## Dependencies
- ffmpeg

## Usage 
```
python crop.py -i /path/to/video -v cropvalue [-o]
```
