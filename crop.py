#!/usr/bin/env python3

import argparse
import sys
import os
import shlex


def crop(video, value, overwrite=False):
    path, extention = video.split(os.extsep, 1)
    output = f'{path} - cropped.{extention}'
    os.system(f'ffmpeg -i {video} -vf "crop={value}" -c:a copy {output}')

    if overwrite:
        os.system(f'rm {video} && mv {output} {video}')


def main():
    parser = argparse.ArgumentParser(description="Remove the black bars from a video.")
    parser.add_argument("-i", "--input", metavar="VIDEO", required="true", help="The video to crop.")
    parser.add_argument("-v", "--cropvalue", required="true", help="The value retrieved from cropdetect (eg. 608:1072:656:4)")
    parser.add_argument("-o", "--overwrite", action="store_true", help="Wether to overwrite the original video. Makes a cropped copy by default.")
    args = parser.parse_args()

    #display help message when no args are passed.
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)

    crop(shlex.quote(args.input), args.cropvalue, args.overwrite)


if __name__ == "__main__":main()